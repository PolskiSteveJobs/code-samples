﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ObjectMovement : MonoBehaviour, IMoveable {

    NavMeshAgent agent;
    Vector3 destination;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    public void MoveTo(Vector3 targetPosition)
    {
        destination = targetPosition;
    }

    void Update()
    {
        if (destination != Vector3.zero)
            agent.SetDestination(destination);

    }
}
