﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Movement : MonoBehaviour {

    IMoveable gameObjectToMove;


	void Update ()
    {
		if(Input.GetMouseButtonDown(0))
        {
            SelectObject();
        }

        if(Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            Vector3 targetPosition;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hit);
            targetPosition = hit.point;
            MoveToTargetPosition(gameObjectToMove, targetPosition);
        }

	}

    void SelectObject()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out hit);
        if(hit.collider.gameObject.CompareTag("Moveable"))
        {
            gameObjectToMove = hit.collider.gameObject.GetComponent<IMoveable>();
        }
        else
        {
            print("You can not move this object");
        }
    }

    void MoveToTargetPosition(IMoveable obj, Vector3 destination)
    {
        obj.MoveTo(destination);
    }

}
